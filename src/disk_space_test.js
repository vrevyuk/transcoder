const {exec} = require('child_process');

async function checkOutFreeDiskSpace(mount_point) {
    return new Promise(function (resolve, reject) {
        exec(`df -h --output=size,used,avail,pcent,target`, function (error, out) {
            try {
                if (error) return reject(error);
                console.log(out);
                const strings = out.split('\n');
                const fields = strings[0].split(' ').map(item => item.trim()).filter(item => !!item);
                const values = strings[1].split(' ').map(item => item.trim()).filter(item => !!item);
                // const obj = fields.reduce((acc, item, index) => ({...acc, [item]:values[index]}));
                console.log(fields);
                console.log(values);
                // console.log(obj);
                resolve();
            } catch (e) {
                reject(e);
            }
        })
    })
}

module.exports = checkOutFreeDiskSpace;