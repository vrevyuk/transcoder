const getSettings = require('./db/get-settings');
const path = require('path');
const {inProgress, setError, removeQueue, updateDBfilename} = require('./db/set-queue-status');
const updateVivatDBfilename = require('./db/update-vivat-db-filename');
const startTranscode = require('./ffmpeg_transcoder');
const errorLogger = require('./error-logger');
const {unlink} = require('fs');
const {promisify} = require('util');
const fsUnlink = promisify(unlink);
const freeDiskSpaceChecker = require('./disk_space_test');

module.exports = async function urlHandler(dbRows) {
    const {transcoder} = await getSettings('transcoder');

    return await dbRows.reduce(async function(promise, dbRow) {
        const {finished, total} = await promise;
        let {id, contentId, urlId, src} = dbRow;
        console.log(`\tStarted ${src}`);
        const [server, file] = `${src}`.split('[stb_token]');

        try {
            if (!server || !file) throw new Error(`URL isn't correct.`);
            const [,,serverHost,] = `${server}`.split('/');
            const setting = transcoder[serverHost];
            if (!setting) throw new Error(`There aren't defined settings for ${serverHost} server`);

            const {dataDir, ffmpegCmd, ffmpegCmdAlt, deleteAfterTranscoding, updateDB} = setting;
            await inProgress(id);
            const hlsIndex = await startTranscode(dataDir, ffmpegCmd, ffmpegCmdAlt, deleteAfterTranscoding, file);
            if (updateDB) await updateVivatDBfilename(file, hlsIndex);
            if (updateDB) await updateDBfilename(urlId, [server, hlsIndex].join('[stb_token]'));
            if (deleteAfterTranscoding) await fsUnlink(path.join(dataDir, file));
            await removeQueue(id);
            console.log(`\t${file} ... done`);
            return {finished: finished + 1, total}
        } catch (error) {
            await setError(id, error ? error.message : 'just error');
            console.log(`\t${file} ... error: ${error.message}`);
            return {finished, total};
        }
    }, Promise.resolve({finished: 0, total: dbRows.length}));
};