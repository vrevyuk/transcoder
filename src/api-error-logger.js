const db = require('./db/mysql');

module.exports = async function (props) {
	try {
		await db(`DELETE FROM api_errors WHERE timestamp < DATE_ADD(NOW(), INTERVAL -7 DAY)`);
		await db(`INSERT INTO api_errors SET ?`, [props]);
	} catch (e) {
		console.error(`Error logging: ${e.message}`);
	}
};