const path = require('path');
const {exec} = require('child_process');
const rimraf = require('rimraf');
const fs = require('fs');
const {promisify} = require('util');
const promisedFsStat = promisify(fs.stat);
const promisedFsMkdir = promisify(fs.mkdir);
const promisedRimRaf = promisify(rimraf);
const promisedExec = promisify(exec);

async function startTranscode2(dataDir, ffmpegCmd, ffmpegCmdAlt, deleteAfterTranscoding, filename) {
    const file = path.parse(filename);
    const movieDir = path.join(dataDir, file.dir, file.name);
    // try {
    //     const stats = await promisedFsStat(movieDir);
    //     if (stats.isDirectory()) await promisedRimRaf(movieDir);
    // } catch(e) {}
    // await promisedFsMkdir(movieDir, {recursive: true});

    try {
        await promisedRimRaf(movieDir);
    } catch(e) { console.log(`can't delete dir ${movieDir}`); }

    try {
        await promisedFsMkdir(movieDir, {recursive: true});
    } catch(e) { console.log(`can't create dir ${movieDir}`); }

    const source = path.join(dataDir, file.dir, file.base);
    const destination = `${movieDir}/index.m3u8`;

    try {
        if (!ffmpegCmd) throw new Error(`Not defined ffmpegCmd in config`);
        await promisedExec(
            ffmpegCmd
                .replace('%src%', source)
                .replace('%dst%', destination)
        );
    } catch (e) {
        if (!ffmpegCmdAlt) throw e;
        // new Error(`Not defined ffmpegCmdAlt in config`);
        await promisedExec(
            ffmpegCmdAlt && ffmpegCmdAlt
                .replace('%src%', source)
                .replace('%dst%', destination)
        );
    }

    return `${path.join(file.dir, file.name)}/index.m3u8`;
}

module.exports = startTranscode2;
