const getQueue = require('./db/get-queue');
const urlHandler = require('./url-handler');
const db = require('./db/mysql');
const errorHandler = require('./error-logger');

const {stat, writeFile, unlink} = require('fs');
const {promisify} = require('util');
const fsWriteFile = promisify(writeFile);
const fsUnlink = promisify(unlink);

const serverName = process.env.vod_server_name;

const lockFileName = `${__dirname}/.lock`;
const {version} = require(__dirname + '/../package.json');

stat(lockFileName, async function(error, file) {
    console.log(`App version ${version}`);
    if (!error && file.isFile()) return console.log('The application is already running ...');
    if (error.code !== "ENOENT") throw error;

    await fsWriteFile(lockFileName, '1')
        .then(getQueue.bind(null, serverName))
        .then(urlHandler)
        .then(({finished, total}) => total === 0
            ? console.log(`${new Date().toISOString()} query was empty.`)
            : console.log(`${new Date().toISOString()} successfully done ${finished} of ${total} files`))
        .catch(errorHandler)
        .finally(async () => {
            try {
                await fsUnlink(lockFileName);
            } catch (e) { console.error('Error deleting .lock file')}

            db.closeAll((error) => {
                if (error) return console.log(`${new Date().toISOString()} has exiting error ${error.message}`);
                process.exit(error ? 1 : 0);
            });
        });
});

process.on('SIGINT', function() {
    db.closeAll((error) => {
        if (error) return console.log(`${new Date().toISOString()} has exiting error ${error.message}`);
        process.exit(error ? 1 : 0);
    });
});