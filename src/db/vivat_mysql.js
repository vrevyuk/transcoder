/**
 * Created by Vitaly Revyuk on 3/14/18.
 */

const mysql = require('mysql2');
const debug = require('debug')('app:transcoder');

const pool = mysql.createPool({ 
    connectionLimit: process.env.NODE_ENV === 'production' ? 10 : 1,
    host: process.env.vivat_mysql_host,
    user: process.env.vivat_mysql_user,
    password: process.env.vivat_mysql_password,
    database: process.env.vivat_mysql_db,
});

pool.on('connection', function (connection) {
  // connection.query(`SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));`);
  // connection.query('CALL setSystemPresets();', function(error, result) {
  //   if (error) {
  //     console.error(error)
  //   } else {
  //     debug('Vivat MySQL system presets has successfully set.');
  //   }
  // });
  debug(`Vivat MySQL connection to ${process.env.vivat_mysql_host} established.`, connection.threadId);
});

module.exports = function (query, params) {
  let start = new Date().getTime();
  return new Promise(function (resolve, reject) {
    pool.getConnection(function (error, connection) {
      if (error) return reject(error);
      let sql = connection.query(query, params, function (error, result) {
        debug(`${new Date().getTime() - start} ms: ${sql.sql}`);
        if (error) {
          connection.release();
          debug(sql, error);
          return reject(error);
        }

        resolve(result);
        connection.release();
      });
    })
  });
};

module.exports.startTransaction = function () {
  return new Promise(function (resolve, reject) {
    pool.getConnection(function (error, connection) {
      debug(error);
      if (error) return reject(error);
      connection.beginTransaction(function (error) {
        debug(error);
        if (error) return reject(error);
        resolve(connection);
      });
    })
  });
};

module.exports.queryTransaction = function (connection, query, params) {
  let start = new Date().getTime();
  return new Promise(function (resolve, reject) {
    let sql = connection.query(query, params, function (error, result) {
      debug(`${new Date().getTime() - start} ms: ${sql.sql}`);
      if (error) {
        debug(error);
        reject(error);
      } else {
        resolve(result);
      }
    });
  });
};

module.exports.commitTransaction = function (connection) {
  return new Promise(function (resolve, reject) {
    connection.commit(function (error) {
      if (error) {
        debug(error);
        connection.rollback(function (rollbackError) {
          return reject(rollbackError || error);
        });
      } else {
        connection.release();
        resolve();
      }
    });
  });
};

module.exports.rollbackTransaction = function (connection) {
  return new Promise(function (resolve, reject) {
    connection.rollback(function (error) {
      debug(error);
      connection.release();
      resolve();
    });
  });
};

module.exports.pool = pool;

module.exports.escape = mysql.escape;

module.exports.closeAll = function (cb) {
  pool.end(function (error) {
    typeof cb === "function" && cb(error);
  })
};
