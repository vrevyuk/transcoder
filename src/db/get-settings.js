
const db = require('./mysql');
const errorLogger = require('../error-logger');
const getBrandByCode = require('./get-brand-by-code');

/**
 *
 * @param keys Array of Strings .... e.g. 'one', 'two', 'three'
 * @return {Promise.<*|VALUE>} // Object with properties that has name like input keys e.g. { 'one': ..., 'two': ..., 'three': ... }
 */
module.exports = async function (...keys) {
	let brandId;
	let props = Array.from(keys);
	if (typeof props[props.length - 1] === 'number') {
		brandId = props.pop();
	} else {
		// determine brandId
		brandId = await getBrandByCode(process.env.brand);
	}

	let query = `SELECT setting_key, setting_value, value_type 
	FROM settings 
		WHERE setting_key IN (?) AND FIND_IN_SET(?, brand)`;

	let rows = await db(query, [keys, brandId]);

	function getValue(row = {}) {
		let { value_type, setting_value } = row;

		switch (value_type) {
			case 'INT': return parseInt(setting_value, 10);
			case 'FLOAT': return parseFloat(setting_value);
			case 'BOOL': return /^true$/i.test(setting_value);
			case 'JSON':
				try {
					return JSON.parse(setting_value);
				} catch (e) {
					errorLogger(e);
					return null;
				}
			default: return `${setting_value}`;
		}
	}

	return await rows.reduce(
		(promise, row) => promise
			.then(
				result => ({
					...result,
					[row['setting_key']]: getValue(row)
				})
			),
		Promise.resolve({})
	);
};