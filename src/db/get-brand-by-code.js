const db = require('./mysql');

module.exports = async function(brand) {
    let [{id: brandId = 0} = {}] = await db('SELECT id FROM brand WHERE code = ?', [brand]);
    return brandId;
}
