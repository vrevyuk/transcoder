const db = require('./mysql');

module.exports = async function(serverName) {
    const query = `SELECT 
    ut.*, cu.src
    FROM url2transcode ut, contentUrls cu
        WHERE ut.urlId = cu.id 
            AND ut.status = 1 
            AND (cu.src LIKE CONCAT('http://', ?, '%') OR cu.src LIKE CONCAT('https://', ?, '%'))
            AND cu.src LIKE '%.mp4'
    ORDER BY ut.status, cu.id        
    LIMIT 0, 10`;

    const rows = await db(query, [serverName, serverName]);
    console.log(`Found ${rows.length} rows.`);
    return rows;
};