const db = require('./vivat_mysql');

module.exports = async function updateVivatDBfilename(oldFile, newFile) {
  const query = `UPDATE add_video_url SET url = REPLACE(url, ?, ?) WHERE url LIKE ?;`;
  await db(query, [oldFile, newFile, `%${oldFile}%`]);
};