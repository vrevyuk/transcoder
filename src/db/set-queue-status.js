const db = require('./mysql');

module.exports.inProgress = async function inProgress(id) {
    const query = `UPDATE url2transcode SET status = 2 WHERE id = ?`;
    return await db(query, [id]);
}

module.exports.setError = async function setError(id, description) {
    const query = `UPDATE url2transcode SET status = 3, description = ? WHERE id = ?`;
    return await db(query, [description, id]);
}

module.exports.removeQueue = async function removeQueue(id) {
    const query = `DELETE FROM url2transcode WHERE id = ?`;
    return await db(query, [id]);
}

module.exports.updateDBfilename = async function updateDBfilename(id, filename) {
    const query = `UPDATE contentUrls SET src = ? WHERE id = ?`;
    return await db(query, [filename, id]);
}