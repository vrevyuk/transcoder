const dbLogger = require('./api-error-logger');

module.exports = async function (error = new Error('has no error'), critical) {

	let typicalError = new Error('internal server error');
	typicalError.status = 500;

	let criticalError = new Error('critical internal server error');
	criticalError.status = 500;

	try {

		let props = {
			name: `${error.name}`,
			message: `${error.sqlMessage || error.message}`,
			stack: `${error.sql || error.stack}`,
		};

		if (/develop/.test(process.env.NODE_ENV)) console.error(error);

		await dbLogger(props);

		return critical ? criticalError : typicalError;

	} catch (e) {
		console.error(e);
		return criticalError;
	}

};